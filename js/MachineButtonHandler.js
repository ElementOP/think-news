//handles buttons from machines page
$(document).ready(function() {
    $('#laptops').click(function() { sortTips('Lap'); }); //Just passing variables here to tell the database handler what to display.
    $('#desktops').click(function() { sortTips('Center'); });
    $('#workstations').click(function() { sortTips('Station'); });
    $('#software').click(function() { sortTips('Software'); });
    $('#docks').click(function() { sortTips('Docks'); });
    $("#searchbar").keyup(function() { //checks to see if the user updates the field and hits enter
        search($(this).val()); //this refers to searchbar
    });
});