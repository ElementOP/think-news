$(document).ready(function() {
    $('#secret').click(function() {
        if (!$(".gamewindow").length) {
            createGameWindow();
            startGame();
        }
    });
});
/*
SECRET INVADERS _ Written by Steven D.
This Script can be added to Any webpage that has Jquery linked and a body element and a clickable element with and id of secret. 
Objective: Survive as long as possible.
A set number of enemies will be sent towards the player and they need to avoid them. The longer the player survives the harder the 
game becomes
TO - DO:
Change Enemy Movement. - Done
Add multiple enemy types - Done
Add limit to bullets - Removed
Handle Game States -
Change Generation of enemies - Done
*/
const exitwindow = () => {
    closeGameWindow();
    console.log(myGameArea.state);
    clearInterval(waveInterval);
    clearInterval(gameInterval);
}
const createGameWindow = () => {
    let $mybody = "<div class='gamewindow'><a class='exit' onClick='exitwindow()' >X</a><canvas id='mycanvas' class='game' height='600' width='600'></canvas></div>";
    $(document.body).append($mybody);
    $('.exit').css({ "color": "White", "margin-left": "1em", "font-size": "2em", "padding-top": "2em" });
    $('.gamewindow').css("position", "fixed");
    $('.gamewindow').css("background-color", "rgba(0,0,0,.5");
    $('.gamewindow').css({ "top": "10vh", "bottom": "10vh", "left": "0", "right": "0" });
    $('.gamewindow').css({ "height": "80%", "width": "100%" });
    $('.game').css("background-color", "rgba(255,255,255,1");
    $('.game').css({ "display": "block", "position": "absolute", "margin": "auto", "top": "0", "bottom": "0", "left": "0", "right": "0" });
}
const closeGameWindow = () => {
    $('.gamewindow').css("display", "none");
    $(".gamewindow").remove();
    GameWindowIsopen = false;
};
const startGame = () => {
    myGameArea = {};
    let myGamePiece = {};
    let myEnemies = {};
    myGameArea = new GameArea();
    myGamePiece = new Component((myGameArea.canvas.width / 2), 550);
    myEnemies = createEnemies();
    myGameArea.state = "Ready";
    myGameArea.initStar();
    window.addEventListener('keydown', function(e) {
        myGameArea.key = (myGameArea.key || []);
        myGameArea.key[e.keyCode] = true;
    })
    window.addEventListener('keyup', function(e) {
        myGameArea.key[e.keyCode] = false;
    })
    if (myGameArea.state == "Ready") {
        myGameArea.state = "Menu";
        gameInterval = setInterval(function() { updateGameArea(myGameArea, myGamePiece, myEnemies) }, 20);
    }
}
class GameArea {
    constructor() {
        this._state = "Ready";
        this.canvas = document.getElementById("mycanvas");
        this.context = this.canvas.getContext("2d");
        this.key;
        this.stars = [];
        this.numStars = 100;
        this.speed = 10;
        this.wave = 1;
        this.score = 0;
        this._quitstate = "Retry";
        this.framealpha = 0;
        this.framevalue = .01;
    }
    clear() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
    drawBackground() {
        this.context.fillStyle = "Black";
        this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
    }
    makeStar() {
        return {
            x: Math.random(),
            y: Math.random(),
            distance: Math.sqrt(Math.random()),
            color: 'hsl(' + Math.random() * 40 + ',100%,' + (70 + Math.random() * 30) + '%)'
        };
    }
    initStar() {
        for (let i = 0; i < this.numStars; i++) {
            this.stars[i] = this.makeStar();
        }
    }
    updateStars() {
        for (let i = 0; i < this.numStars; i++) {
            this.stars[i].y -= Math.pow(this.stars[i].distance, 2) / this.canvas.width * this.speed;
            if (this.stars[i].y <= 0) {
                this.stars[i] = this.makeStar();
                this.stars[i].y = 1;
            }
            this.context.beginPath();
            this.context.arc(this.stars[i].x * this.canvas.width, this.stars[i].y * this.canvas.height, this.stars[i].distance * 2, 0, 2 * Math.PI, false);
            this.context.lineWidth = this.stars[i].distance * 4;
            this.context.strokeStyle = 'rgba(255,255,255,0.2)';
            this.context.stroke();
            this.context.fillStyle = this.stars[i].color;
            this.context.fill();
        }
    }
    updatescore(points) {
        this.score += points;

    }
    showscore() {
        this.context.fillStyle = "white";
        this.context.font = "15px Arial";
        this.context.fillText("Score: " + this.score, 0, 15);
        this.context.fillText("Wave: " + this.wave, 0, 30);
    }
    showMainMenu() {
        this.context.fillStyle = "Green";
        this.context.font = "45px Arial";
        this.context.fillText("Secret Invaders", this.canvas.width / 2 - 160, this.canvas.height / 2 - 45);
        this.context.font = "20px Arial";
        this.context.fillStyle = "rgba(255, 0, 0, " + this.framealpha + ")";
        this.context.fillText("Press Space to start", this.canvas.width / 2 - 90, this.canvas.height / 2);
        if (this.framealpha >= 0) {
            this.framealpha += this.framevalue;
            if (this.framealpha >= 1 || this.framealpha < .01) {
                this.framevalue = this.framevalue * -1;
            }
        }
    }
    showGameOver() {
        this.context.fillStyle = "Green";
        this.context.font = "30px Arial";
        this.context.fillText("Your Score: " + this.score, this.canvas.width / 2 - 100, this.canvas.height / 2 - 45);
        this.context.fillText("Game Over", this.canvas.width / 2 - 60, this.canvas.height / 2);
        this.context.fillText("Retry", this.canvas.width / 2 - 30, this.canvas.height / 2 + 30);
        this.context.fillText("Quit", this.canvas.width / 2 - 30, this.canvas.height / 2 + 60);
    }
    set state(newstate) {
        this._state = newstate;
    }
    get state() {
        return this._state;
    }
    showGameOverCursor() {
        if (this._quitstate == "Retry") {
            this.context.fillStyle = this.color;
            this.context.beginPath();
            this.context.moveTo(this.canvas.width / 2 - 20 - 30, this.canvas.height / 2 + 30);
            this.context.lineTo(this.canvas.width / 2 - 20 - 30, this.canvas.height / 2 + 5);
            this.context.lineTo(this.canvas.width / 2 - 5 - 30, this.canvas.height / 2 + 18);
            this.context.lineTo(this.canvas.width / 2 - 20 - 30, this.canvas.height / 2 + 30);
            this.context.closePath();
            this.context.fill();
        }
        if (this._quitstate == "Quit") {
            this.context.fillStyle = this.color;
            this.context.beginPath();
            this.context.moveTo(this.canvas.width / 2 - 20 - 30, this.canvas.height / 2 + 60);
            this.context.lineTo(this.canvas.width / 2 - 20 - 30, this.canvas.height / 2 + 35);
            this.context.lineTo(this.canvas.width / 2 - 5 - 30, this.canvas.height / 2 + 50);
            this.context.lineTo(this.canvas.width / 2 - 20 - 30, this.canvas.height / 2 + 60);
            this.context.closePath();
            this.context.fill();
        }
    }
    handleGameOver() {
        if (this._quitstate == "Retry") {
            this._state = "Reset";
        }
        if (this._quitstate == "Quit") {
            this._state = "Quit";
        }
    }
    set quitstate(flip) {
        this._quitstate = flip;
    }
    increaseWave() {
        this.wave = Math.round((this.wave + .1) * 100) / 100;
        console.log(this.wave)
    }
    reset() {
        this.wave = 1;
        this.score = 0;
        this._state = "Play";
    }
}
class Component {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.w = 20;
        this.h = 20;
        this.speedX = 0;
        this.speedY = 0;
        this.alive = true;
        this.bx = x + 8;
        this.by = y + 10;
        this.bw = 6;
        this.bh = 6;
    }
    update(ctx) {
        ctx.fillStyle = "green";
        ctx.beginPath();
        ctx.moveTo(11 + this.x, 0 + this.y);
        ctx.lineTo(13 + this.x, 10 + this.y);
        ctx.lineTo(21 + this.x, 17 + this.y);
        ctx.lineTo(21 + this.x, 20 + this.y);
        ctx.lineTo(13 + this.x, 17 + this.y);
        ctx.lineTo(13 + this.x, 20 + this.y);
        ctx.lineTo(9 + this.x, 20 + this.y);
        ctx.lineTo(9 + this.x, 17 + this.y);
        ctx.lineTo(0 + this.x, 20 + this.y);
        ctx.lineTo(0 + this.x, 17 + this.y);
        ctx.lineTo(9 + this.x, 10 + this.y);
        ctx.lineTo(11 + this.x, 0 + this.y);
        ctx.closePath();
        ctx.fill();
        ctx.fillStyle = "red";
        ctx.fillRect(this.bx, this.by, this.bw, this.bh);
    }
    newPos(canvas) {
        this.x += this.speedX;
        this.y += this.speedY;
        this.bx = this.x + 8;
        this.by = this.y + 10;
        if (this.x < 0) {
            this.x = 0;
            this.bx = this.x + 8;
        }
        if (this.x + this.w > canvas.width) {
            this.x = canvas.width - this.w;
            this.bx = this.x + 8;
        }
        if (this.y < 0) {
            this.y = 0;
            this.by = this.y + 10;
        }
        if (this.y + this.h > canvas.height) {
            this.y = canvas.height - this.h;
            this.by = this.y + 10;
        }
    }
    checkCollision(obj) {
        if (doCollision(this, obj)) {
            this.alive = false;
        }
    }
    reset(canvas) {
        this.x = canvas.width / 2;
        this.y = 550;
        this.bx = this.x + 8;
        this.by = this.y + 10;
        this.alive = true;
    }
}
class Enemy {
    constructor(x, y, etype) {
        this.x = x;
        this.y = y;
        this.w = 20;
        this.h = 20;
        this.speedX = 0;
        this.speedY = 1;
        this.alive = true;
        this.colors = ["blue", "red", "yellow"];
        this.type = etype;
        this.bx = x + 5;
        this.by = y;
        this.bw = 10;
        this.bh = 10;
    }
    update(ctx) {
        ctx.fillStyle = this.colors[this.type];
        ctx.beginPath();
        ctx.moveTo(0 + this.x, 0 + this.y);
        ctx.lineTo(0 + this.x, 10 + this.y);
        ctx.lineTo(4 + this.x, 10 + this.y);
        ctx.lineTo(4 + this.x, 6 + this.y);
        ctx.lineTo(8 + this.x, 6 + this.y);
        ctx.lineTo(8 + this.x, 18 + this.y);
        ctx.lineTo(10 + this.x, 20 + this.y);
        ctx.lineTo(12 + this.x, 18 + this.y);
        ctx.lineTo(12 + this.x, 6 + this.y);
        ctx.lineTo(16 + this.x, 6 + this.y);
        ctx.lineTo(16 + this.x, 10 + this.y);
        ctx.lineTo(20 + this.x, 10 + this.y);
        ctx.lineTo(20 + this.x, 0 + this.y);
        ctx.lineTo(12 + this.x, 4 + this.y);
        ctx.lineTo(8 + this.x, 4 + this.y);
        ctx.lineTo(0 + this.x, 0 + this.y);
        ctx.closePath();
        ctx.fill();
    }
    reset() {
        this.alive = true;
        this.x = Math.floor(Math.random() * 600);
        this.y = Math.floor(Math.random() * 1000) * -1;
        this.bx = this.x + 5;
        this.by = this.y;
        this.speedY = 1;
    }
    newPos(canvas) {
        if (this.type == 0) {
            this.x += Math.sin(this.y / 50)
            this.y += this.speedY;
            this.bx = this.x + 5;
            this.by = this.y;
        }
        if (this.type == 1) {
            this.y += this.speedY * 2;
            this.bx = this.x + 5;
            this.by = this.y;
        }
        if (this.type == 2) {
            this.y += this.speedY;
            if (this.y >= 580) {
                this.speedY = -this.speedY;
            }
            if (this.y <= 0 && this.speedY < 0) {
                this.speedY = -this.speedY;
            }
            this.bx = this.x + 5;
            this.by = this.y;
        }
        if (this.y > canvas.height) {
            this.y = Math.floor(Math.random() * 1000) * -1;
            this.x = Math.floor(Math.random() * 600);
            this.bx = this.x + 5;
            this.by = this.y;
        }
    }
}
const updateGameArea = (Gameobj, playerobj, Enemy) => {
    Gameobj.clear();
    Gameobj.drawBackground();
    Gameobj.updateStars();
    if (Gameobj.state == "Reset") {
        playerobj.reset(Gameobj.canvas);
        Enemy.forEach(function(element) {
            element.reset();
        })
        clearInterval(waveInterval);
        waveInterval = setInterval(function() { Gameobj.increaseWave() }, 3000);
        Gameobj.reset();
    }
    if (Gameobj.state == "Menu") {
        Gameobj.showMainMenu();
        if (Gameobj.key && Gameobj.key[32]) {
            Gameobj.state = "Play";
            waveInterval = setInterval(function() { myGameArea.increaseWave() }, 3000);
        }
    }
    if (Gameobj.state == "Quit") {
        clearInterval(gameInterval);
        clearInterval(waveInterval);
        closeGameWindow();
    }
    if (Gameobj.state == "GameOver") {
        Gameobj.showGameOver();
        Gameobj.showGameOverCursor();
        if (Gameobj.key[38]) { Gameobj.quitstate = "Retry" }
        if (Gameobj.key[40]) { Gameobj.quitstate = "Quit" }
        if (Gameobj.key[32]) { Gameobj.handleGameOver(); }
    }
    if (Gameobj.state == "Play" || Gameobj.state == "Pause") {
        Gameobj.showscore();
        playerobj.speedX = 0;
        playerobj.speedY = 0;
        if (playerobj.alive == true) {
            if (Gameobj.key && Gameobj.key[37]) { playerobj.speedX = -(5 + Gameobj.wave); }
            if (Gameobj.key && Gameobj.key[39]) { playerobj.speedX = 5 + Gameobj.wave; }
            if (Gameobj.key && Gameobj.key[38]) { playerobj.speedY = -(5 + Gameobj.wave); }
            if (Gameobj.key && Gameobj.key[40]) { playerobj.speedY = 5 + Gameobj.wave; }
        }
        if (playerobj.alive == true) {
            playerobj.newPos(Gameobj.canvas);
            Enemy.forEach(function(element) {
                playerobj.checkCollision(element);
            })
        }
        Enemy.forEach(function(element) {
            if (element.alive == true) {
                element.newPos(Gameobj.canvas);
                element.update(Gameobj.context);
                element.speedY = Gameobj.wave;
            }
        })
        if (playerobj.alive == true) {
            playerobj.update(Gameobj.context);
        }
        if (playerobj.alive) {
            Gameobj.updatescore(1);
        } else {
            Gameobj.state = "GameOver";
        }
    }
}

const createEnemies = () => {
    let TempArray = [];
    let w = 20;
    let h = 20;
    let x = 10;
    let y = 0;
    let maxEnemies = 100;
    for (let i = 0; i < maxEnemies; i++) {
        let x = Math.floor(Math.random() * 600);
        let y = Math.floor(Math.random() * 1000) * -1;
        let type = Math.floor(Math.random() * 3);
        TempArray.push(new Enemy(x, y, type));
    }
    return TempArray;
}

const doCollision = (rect1, rect2) => {
    if (rect1.bx < rect2.bx + rect2.bw && rect1.bx + rect1.bw > rect2.bx && rect1.by < rect2.by + rect2.bh && rect1.bh + rect1.by > rect2.by) {
        return true;
    } else {
        return false;
    }
}