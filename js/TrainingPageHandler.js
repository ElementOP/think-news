$(document).ready(function() { // this is jquery
    $('.upper-part').click(function() {
        $(this).parent().siblings().children().next().slideUp();
        $(this).siblings(".lower-part").slideToggle("slow"); //exapnds the list downward works on all class entry dom objects
    });
    $("#searchbar").keyup(function() {
        search($(this).val());
    });
});

const search = (searchterm) => { //super specail search function
    let upperSearchTerm = searchterm.toUpperCase();
    $("a").each(function() {
        let acontainer = $(this).html()
        if (acontainer.search(upperSearchTerm)) {
            $(this).hide();
        } else {
            $(this).show();
        }
    });
};