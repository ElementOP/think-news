//Handler for button clicks

$(document).ready(function() { // this is jquery
    FirefoxFix();
    $('#main-iframe').attr('src', './pages/updates.html');
    $('#Updates').click(function() { //if the techtips button is clicked on
        currentTab($(this));
        $('#main-iframe').attr('src', './pages/updates.html');
    });
    $('#Techtips').click(function() { //if the techtips button is clicked on
        //$('#main-iframe').attr('srcdoc',' ');
        $('#main-iframe').attr('src', './pages/machines.html');
        currentTab($(this));
    });
    $('#Process').click(function() { //if the Process button is clicked on
        $('#main-iframe').attr('src', './pages/process.html');
        currentTab($(this));
    });
    $('#Links').click(function() { //if the Links
        $('#main-iframe').attr('src', './pages/links.html');
        currentTab($(this));
    });
    $('#Legacy').click(function() { //if the legacy button is clicked on.
        window.open("../L_Thinknews/index.htm");
        currentTab($(this));
    });
    $('#Bios').click(function() { //if the legacy button is clicked on.
        $('#main-iframe').attr('src', 'https://download.lenovo.com/bsco/index.html');
        currentTab($(this));
    });
    $('#Documents').click(function() { //if the legacy button is clicked on.
        $('#main-iframe').attr('src', './pages/documents.html');
        currentTab($(this));
    });
    $('#Training').click(function() { //if the legacy button is clicked on.
        $('#main-iframe').attr('src', './pages/training.html');
        currentTab($(this));
    });
    $('#SSR').click(function() { //if the legacy button is clicked on.
        $('#main-iframe').attr('src', './pages/ssr.html');
        currentTab($(this));
    });
});

const currentTab = (tab) => {
    tab.css("background-color", "#2256b5");
    tab.siblings().css("background-color", "rgba(49, 122, 239, 0.82)");
};

const FirefoxFix = () => { //checks to see if the browser is firefox and is updated because of css grids
    let isFirefox = typeof InstallTrigger !== 'undefined';
    if (isFirefox) {
        let match = navigator.userAgent.match(/Firefox\/([0-9]+)\./);
        let ver = match ? parseInt(match[1]) : 0;
        if (ver < "55") {
            $('.popup').show();
        }
    }
};