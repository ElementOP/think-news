$(document).ready(function() {
    $('.link').click(function() {
        setIframe($(this).attr("title")); //when you click any of the class links it puts the link into the main iframe
    });
    $('.exit').click(function() {
        $('.popup').hide();
    });
});

const setIframe = (value) => {
    $('iframe').attr('src', value);
    $('.popup').show();
};