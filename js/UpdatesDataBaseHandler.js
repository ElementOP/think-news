//This whole thing basically creates a webpage and sends it to the iframe on the main page.

$(document).ready(function() {
    $.updates = new Array;
    loadXMLDocUpdateDB($.updates);
});

const loadXMLDocUpdateDB = (array) => {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) { //Need a delay to make sure the file is ready
            loadUpdates(this, array); // 'this' is the xml request
        }
    };
    xmlhttp.open("GET", "../database/siteupdatedata.xml", true); //file database that hold all of the updates
    xmlhttp.setRequestHeader('Cache-Control', 'no-cache');
    xmlhttp.send();
};

const loadUpdates = (xml, array) => { //puts database input into a var Entries and then replaces the html of the div site-updates
    let xmlDoc = xml.responseXML;
    let x = xmlDoc.getElementsByTagName("Entry"); // Grabs every element inside of the table with the tag entry
    let title, date, symptom, solution, info, list, image, imageLink, linkName, link;
    for (let i = 0; i < x.length; i++) {
        link = linkName = list = [];
        title = date = symptom = solution = info = image = imageLink = "";
        if (x[i].getElementsByTagName("Title").length) {
            title = x[i].getElementsByTagName("Title")[0].childNodes[0].nodeValue;
        }
        if (x[i].getElementsByTagName("Date").length) {
            date = x[i].getElementsByTagName("Date")[0].childNodes[0].nodeValue;
        }
        if (x[i].getElementsByTagName("Symptom").length) {
            symptom = x[i].getElementsByTagName("Symptom")[0].childNodes[0].nodeValue;
        }
        if (x[i].getElementsByTagName("Solution").length) {
            solution = x[i].getElementsByTagName("Solution")[0].childNodes[0].nodeValue;
        }
        if (x[i].getElementsByTagName("Info").length) {
            info = x[i].getElementsByTagName("Info")[0].childNodes[0].nodeValue;
        }
        if (x[i].getElementsByTagName("List").length) {
            let listEntry = x[i].getElementsByTagName("List")[0].childNodes[0].nodeValue;
            list = listEntry.split("/");
        }
        if (x[i].getElementsByTagName("Image").length) {
            image = x[i].getElementsByTagName("Image")[0].childNodes[0].nodeValue;
        }
        if (x[i].getElementsByTagName("ImageLink").length) {
            imageLink = x[i].getElementsByTagName("ImageLink")[0].childNodes[0].nodeValue;
        }
        if (x[i].getElementsByTagName("LinkName").length) {
            let thisLink = x[i].getElementsByTagName("LinkName")[0].childNodes[0].nodeValue;
            linkName = thisLink.split(',');
        }
        if (x[i].getElementsByTagName("Link").length) {
            let getLink = x[i].getElementsByTagName("Link")[0].childNodes[0].nodeValue;
            link = getLink.split(" ");
        }
        array.push({ Title: title, Date: date, Symptom: symptom, Solution: solution, Info: info, List: list, Image: image, ImageLink: imageLink, LinkName: linkName, Link: link });
    }
    renderUpdates(array, true);
};
const renderUpdates = (array, filterSwitch) => {
    if (!array) {
        array = $.updates;
    }
    let today = new Date(); // get the current date;
    today.setMonth(today.getMonth() - 6); // subtracts 3 months from the current date
    let _dbdate = ""; //place holder for date pulled from database
    let Entries = "<div class='tiptable'>"; //created a class for ccss so it is easirer to style.
    array.forEach(function(item) {
        let thisDate = stringToDate(item.Date, "mm/dd/yyyy", "/");
        if (thisDate.getTime() - today.getTime() > 0 || !filterSwitch) {
            Entries += "<div class = 'entry'>";
            Entries += item.Title ? `<p class= 'title'>${item.Title}</p>` : "";
            Entries += "<div class='forborder'>";
            Entries += item.Date ? `<p class='Blue'>${item.Date}</p>` : "";
            Entries += item.Symptom ? `<p class='Blue'>Symptom: </p><p>${item.Symptom}</p>` : "";
            Entries += item.Solution ? `<p class='Blue'>Solution: </p><p>${item.Solution}</p>` : "";
            Entries += item.Info ? `<p>${item.Info}</p>` : "";
            Entries += item.List ? "<ol>" + item.List.map(function(listitem) { return `<li>${listitem}</li>` }).join(" ") + "</ol>" : "";
            Entries += item.Image ? item.ImageLink ? `<a  href='${item.ImageLink}'><img class='inserted' src ='${item.Image}'></img></a>` : `<img class='inserted' src ='${item.Image}'>` : "";
            if (item.Link) {
                if (item.LinkName.length > 0) {
                    Entries += item.LinkName.map(function(linkentry) { return `<a class='links' href='${item.Link[item.LinkName.indexOf(linkentry)]}'>${linkentry}</a>` }).join();
                } else {
                    Entries += item.Link.map(function(link) { return `<a class='links' href='${link}'>${link}</a>` }).join("");
                }
            }
            Entries += "</div>";
        }
    });
    Entries += "</div>";
    document.getElementById("Updates").innerHTML = Entries; // replaces the elements html with the fully created tiptable div.
};
const stringToDate = (_date, _format, _delimiter) => { // got this from stack overflow to convert the string into a usable date. https://stackoverflow.com/questions/5619202/converting-string-to-date-in-j{
    let formatLowerCase = _format.toLowerCase();
    let formatItems = formatLowerCase.split(_delimiter);
    let dateItems = _date.split(_delimiter);
    let monthIndex = formatItems.indexOf("mm");
    let dayIndex = formatItems.indexOf("dd");
    let yearIndex = formatItems.indexOf("yyyy");
    let yearNumber = Number(dateItems[yearIndex]);
    if (yearNumber < 2000) { //Fixed a Glaring issue with date formatting. 
        yearNumber += 2000;
    }
    let month = parseInt(dateItems[monthIndex]);
    month -= 1;
    let formatedDate = new Date(yearNumber.toString(), month, dateItems[dayIndex], 0, 0, 0);
    return formatedDate;
};