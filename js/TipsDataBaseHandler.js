//this page will handle loading and sorting of info for the tips database
let tipArray = []; //create an array for tips
$(document).ready(function() {
    loadXMLDocTipsDB(); //loads the information from the database into tipArray
});

const loadXMLDocTipsDB = () => {
    const xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) { //Need a delay to make sure the file is ready
            loadTips(this); // 'this' is the xml request
        }
    };
    xmlhttp.open("GET", "../database/tipsdata.xml", true); //file database that hold all of the updates
    xmlhttp.setRequestHeader('Cache-Control', 'no-cache'); // Dom is static and browsers like to store.
    xmlhttp.send();
};

const loadTips = (xml) => { //puts database input into a let Entries and then replaces the html of the div site-updates
    const xmlDoc = xml.responseXML;
    const x = xmlDoc.getElementsByTagName("Entry"); // Grabs every element inside of the table with the tag entry
    let title, date, type, link, key;
    for (let i = 0; i < x.length; i++) {
        if (x[i].getElementsByTagName("Title").length) {
            title = x[i].getElementsByTagName("Title")[0].childNodes[0].nodeValue; // everything is saved into a variable and the pushed into an array
        }
        if (x[i].getElementsByTagName("Date").length) {
            date = x[i].getElementsByTagName("Date")[0].childNodes[0].nodeValue;
        }
        if (x[i].getElementsByTagName("Type").length) {
            type = x[i].getElementsByTagName("Type")[0].childNodes[0].nodeValue;
        }
        if (x[i].getElementsByTagName("Link").length) {
            link = x[i].getElementsByTagName("Link")[0].childNodes[0].nodeValue;
        }
        if (x[i].getElementsByTagName("Meta").length) {
            let tempstringformeta = "";
            let y = x[i].getElementsByTagName("Key"); // parsing through everything in the Meta tag
            for (let j = 0; j < y.length; j++) {
                tempstringformeta += y[j].childNodes[0].nodeValue;
            }
            key = tempstringformeta;
        }
        tipArray.push({ Title: title, Date: date, Type: type, Link: link, Key: key }); // push the info into an array for processing and sorting
    }
    sortTips(); //calls sort tips here in order to actually have the data displayed, xml requests take a second so tip array will be empty if called before this function
};

const sortTips = (type) => {
    let Entries = ""; // creating a variable to store a div containing the elements we search for!
    Entries += "<base target='_blank' /><div class='sorted'>"; //base targets new is needed because we want to open the links in a new tab
    tipArray.forEach(function(item) { //this has been completely reworked to make it easier to read before I was running for loops on each case but its less intensive to run 1 forEach.
        switch (type) {
            case 'All':
                Entries += concatText(item);
                break;
            case 'Lap':
                (item.Type === 'ThinkPad' || item.Type === 'All') ? Entries += concatText(item): false; //if type is thinkPad or All pass the item to the concat function to return a block of html
                break;
            case 'Center':
                (item.Type === 'ThinkCentre' || item.Type === 'All') ? Entries += concatText(item): false; //same
                break;
            case 'Software':
                (item.Type === 'OS') ? Entries += concatText(item): false;
                break;
            case 'Station':
                (item.Type === 'ThinkStation' || item.Type === 'All') ? Entries += concatText(item): false;
                break;
            case 'Docks':
                (item.Type === 'Docks') ? Entries += concatText(item): false;
                break;
            default: //catch all for items that do not have a type or if sort does not pass the correct item.
                Entries += concatText(item);
                break;
        }
    });
    Entries += "</div>";
    $('#machines').html(Entries); //sets the html of the machines id div to the variable with all the info saved.
};

const search = (searchterm) => { //this is the search function 
    let searchArray = []; // initalize an array to hold all of our searched terms
    let Entries = "<base target='_blank' /><div class='sorted'>"; // creating a variable to store a div containing the elements we search for
    searchArray = tipArray.filter(function(node) { //filter takes our searched array and assigns each node of tipArray that meets either of the two condition either its in Title or Key;
        return node.Title.toLowerCase().search(searchterm.toLowerCase()) > -1 || node.Key.toLowerCase().search(searchterm.toLowerCase()) > -1;
    });
    searchArray.forEach(function(item) { //goes through each item in the array and concats it into the Entries variable;
        Entries += concatText(item); // I am so glad I wrote this function saves time and space! '_'
    });
    Entries += "</div>"; //end the div
    $('#machines').html(Entries); //sets the html of the machines id div to the variable with all the info saved.
};

const concatText = (passed) => { //takes an object as Object{Title:, Date:, Link:} and concats them into a blocl "entry of HTML to be added to the DOM"
    return `<h3 id='title'>${passed.Title}</h3>
<div class='forborder'><p id='date'>Date:</p>&emsp;<p>${passed.Date}</p>
<a href ='${passed.Link}' target='_blank'>Click here</a></div>`;
};