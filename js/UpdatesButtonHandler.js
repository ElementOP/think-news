$(document).ready(function() {
    //loadXMLDocUpdateDB();
    currentTab($('#filter'));
    $('#filter').click(function() {
        currentTab($(this));
        renderUpdates("", true);
    });
    $('#all').click(function() {
        currentTab($(this));
        renderUpdates("", false);
    });
});

const currentTab = (tab) => {
    tab.css("background-color", "#2256b5");
    tab.siblings().css("background-color", "rgba(49, 122, 239, 0.82)");
};