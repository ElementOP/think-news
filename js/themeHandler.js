$(document).ready(function() {
    setTheme(getDate());
    $("#main-iframe").on('load', function() {
        setUpIframe($(this), getDate());
    });
});

const getDate = () => {
    let date = new Date();
    let currentmonth = date.getMonth();
    return currentmonth;
}
const Seasons = {
    spring: "rgba(1, 248, 42, 0.62)",
    summer: "rgba(253, 215, 0, 0.82)",
    fall: "rgba(216, 77, 42, 0.77)",
    winter: "rgba(94, 202, 245, 0.62)",
    springimg: "./resources/images/Backgrounds/spring.jpeg",
    summerimg: "./resources/images/Backgrounds/summer.jpeg",
    fallimg: "./resources/images/Backgrounds/fall.jpeg",
    winterimg: "./resources/images/Backgrounds/winter.jpeg"
};

const setTheme = (date) => {
    let currentmonth = date + 1;
    if (currentmonth > 11) {
        currentmonth = 0;
    }

    myMonths = [Seasons.winter, Seasons.winter, Seasons.winter, Seasons.spring, Seasons.spring, Seasons.spring, Seasons.summer, Seasons.summer, Seasons.summer, Seasons.fall, Seasons.fall, Seasons.fall];
    let myMonthsImg = [Seasons.winterimg, Seasons.winterimg, Seasons.winterimg, Seasons.springimg, Seasons.springimg, Seasons.springimg, Seasons.summerimg, Seasons.summerimg, Seasons.summerimg, Seasons.fallimg, Seasons.fallimg, Seasons.fallimg];

    $(document.body).css("background-image", `url('${myMonthsImg[currentmonth]}')`);
    $(document.body).css({ "Background-size": "cover", "Background-repeat": "no-repeat" });
    $("#blank-area").css({
        "background": `-webkit-linear-gradient(left, #3174EF, ${myMonths[currentmonth]}`,
        "background": `linear-gradient(to right, #3174EF, ${myMonths[currentmonth]})`
    });
}
const setUpIframe = ($iframe, date) => {
    let currentmonth = date + 1;
    if (currentmonth > 11) {
        currentmonth = 0;
    }
    $iframe.contents().find(".document-title").css({
        "background": `-webkit-linear-gradient(left, #3174EF, ${myMonths[currentmonth]})`,
        "background": `linear-gradient(to right, #3174EF, ${myMonths[currentmonth]}`
    });
    $iframe.contents().find("#document a").css({
        "background": `-webkit-linear-gradient(left, #3174EF, ${myMonths[currentmonth]})`,
        "background": `linear-gradient(to right, #3174EF, ${myMonths[currentmonth]}`
    });
    $iframe.contents().find(".dateselect").css({
        "background": `-webkit-linear-gradient(left, #3174EF, ${myMonths[currentmonth]})`,
        "background": `linear-gradient(to right, #3174EF, ${myMonths[currentmonth]}`
    });
    $iframe.contents().find(".update-title").css({
        "background": `-webkit-linear-gradient(left, #3174EF, ${myMonths[currentmonth]})`,
        "background": `linear-gradient(to right, #3174EF, ${myMonths[currentmonth]}`
    });
    $iframe.contents().find(".process-title").css({
        "background": `-webkit-linear-gradient(left, #3174EF, ${myMonths[currentmonth]})`,
        "background": `linear-gradient(to right, #3174EF, ${myMonths[currentmonth]}`
    });
    $iframe.contents().find("#process a").css({
        "background": `-webkit-linear-gradient(left, #3174EF, ${myMonths[currentmonth]})`,
        "background": `linear-gradient(to right, #3174EF, ${myMonths[currentmonth]}`
    });
    $iframe.contents().find("#title").css({
        "background": `-webkit-linear-gradient(left, #3174EF, ${myMonths[currentmonth]})`,
        "background": `linear-gradient(to right, #3174EF, ${myMonths[currentmonth]}`
    });
    $iframe.contents().find("#links-grid a").css({
        "background": `-webkit-linear-gradient(left, #3174EF, ${myMonths[currentmonth]})`,
        "background": `linear-gradient(to right, #3174EF, ${myMonths[currentmonth]}`
    });
    $iframe.contents().find(".training-title").css({
        "background": `-webkit-linear-gradient(left, #3174EF, ${myMonths[currentmonth]})`,
        "background": `linear-gradient(to right, #3174EF, ${myMonths[currentmonth]}`
    });
    $iframe.contents().find(".upper-part").css({
        "background": `-webkit-linear-gradient(left, #3174EF, ${myMonths[currentmonth]})`,
        "background": `linear-gradient(to right, #3174EF, ${myMonths[currentmonth]}`
    });

    $iframe.contents().find(".ssr-title").css({
        "background": `-webkit-linear-gradient(left, #3174EF, ${myMonths[currentmonth]})`,
        "background": `linear-gradient(to right, #3174EF, ${myMonths[currentmonth]}`
    });
    $iframe.contents().find(".upper-part").css({
        "background": `-webkit-linear-gradient(left, #3174EF, ${myMonths[currentmonth]})`,
        "background": `linear-gradient(to right, #3174EF, ${myMonths[currentmonth]}`
    });
}